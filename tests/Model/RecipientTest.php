<?php

namespace Miniframe\Mailer\Model;

use PHPUnit\Framework\TestCase;

class RecipientTest extends TestCase
{
    /**
     * Tests with just giving an email address
     *
     * @return void
     */
    public function testOnlyEmail(): void
    {
        $recipient = new Recipient('foo@bar.baz');
        $this->assertEquals('foo@bar.baz', $recipient->getEmail());
        $this->assertEquals('Foo', $recipient->getName());
        $this->assertEquals(Recipient::HEADER_TO, $recipient->getHeader());
    }

    /**
     * Tests defining a recipient from state.
     *
     * @return void
     */
    public function testSetState(): void
    {
        $recipient = Recipient::__set_state([
            'email' => 'foo@bar.baz',
            'name' => 'Foo',
            'header' => Recipient::HEADER_TO
        ]);
        $this->assertEquals('foo@bar.baz', $recipient->getEmail());
        $this->assertEquals('Foo', $recipient->getName());
        $this->assertEquals(Recipient::HEADER_TO, $recipient->getHeader());
    }

    /**
     * Tests with an invalid email address
     *
     * @return void
     */
    public function testInvalidEmail(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid email address specified');
        new Recipient('foobarbaz');
    }

    /**
     * Tests with an invalid header value
     *
     * @return void
     */
    public function testInvalidHeader(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The header must be one of the constants HEADER_TO, HEADER_CC or HEADER_BCC');
        new Recipient('foo@bar.baz', 'Foo', 4);
    }
}
