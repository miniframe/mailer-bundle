<?php

namespace Miniframe\Mailer\Model;

use PHPUnit\Framework\TestCase;

class AttachmentTest extends TestCase
{
    /**
     * Tests with a file from disk
     *
     * @return void
     */
    public function testFileFromDisk(): void
    {
        $attachment = new Attachment(__FILE__);
        $this->assertEquals(__FILE__, $attachment->getFullPath());
        $this->assertEquals('AttachmentTest.php', $attachment->getFilename());
        $this->assertEquals(Attachment::DISPOSITION_ATTACHMENT, $attachment->getDisposition());
        $this->assertNull($attachment->getAttachmentData());
        $this->assertEquals('text/x-php', $attachment->getContentType());
    }

    /**
     * Tests defining an attachment from state.
     *
     * @return void
     */
    public function testSetState(): void
    {
        $attachment = Attachment::__set_state([
            'fullPath' => __FILE__,
            'filename' => 'AttachmentTest.php',
            'contentType' => 'text/x-php',
            'disposition' => Attachment::DISPOSITION_ATTACHMENT,
            'attachmentData' => null,
        ]);
        $this->assertEquals(__FILE__, $attachment->getFullPath());
        $this->assertEquals('AttachmentTest.php', $attachment->getFilename());
        $this->assertEquals(Attachment::DISPOSITION_ATTACHMENT, $attachment->getDisposition());
        $this->assertNull($attachment->getAttachmentData());
        $this->assertEquals('text/x-php', $attachment->getContentType());
    }

    /**
     * Tests with a file from memory
     *
     * @return void
     */
    public function testFileFromMemory(): void
    {
        $attachment = new Attachment(null, 'test.txt', 'text/plain', Attachment::DISPOSITION_INLINE, 'foobar');
        $this->assertNull($attachment->getFullPath());
        $this->assertEquals('test.txt', $attachment->getFilename());
        $this->assertEquals('text/plain', $attachment->getContentType());
        $this->assertEquals(Attachment::DISPOSITION_INLINE, $attachment->getDisposition());
        $this->assertEquals('foobar', $attachment->getAttachmentData());
    }

    /**
     * Tests with a file that doesn't exist
     *
     * @return void
     */
    public function testInvalidFile(): void
    {
        $path = __DIR__ . '/notfound.txt';
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('File does not exist: ' . $path);
        new Attachment($path);
    }

    /**
     * Tests with an invalid disposition value
     *
     * @return void
     */
    public function testInvalidDisposition(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Disposition must be one of DISPOSITION_INLINE or DISPOSITION_ATTACHMENT');
        new Attachment(__FILE__, 'foobar', 'text/x-php', 3);
    }

    /**
     * Tests with no file given.
     *
     * @return void
     */
    public function testNoFileGiven(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('When no fullPath is given, filename and attachmentData are required.');
        new Attachment();
    }

    /**
     * Tests with file from memory, and file from disk
     *
     * @return void
     */
    public function testFileDouble(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('When attachmentData is specified, filename must be null.');
        new Attachment(__FILE__, 'foobar', 'text/x-php', Attachment::DISPOSITION_INLINE, 'foobar');
    }
}
