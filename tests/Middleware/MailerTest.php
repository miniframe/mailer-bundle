<?php

namespace Miniframe\Mailer\Middleware;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Mailer\Model\Attachment;
use Miniframe\Mailer\Model\Recipient;
use PHPMailer\PHPMailer\Exception;
use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Builds a config object with specific mailer parameters
     *
     * @param array $mailerData The mailer parameters.
     *
     * @return Config
     */
    protected function buildConfig(array $mailerData): Config
    {
        return Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => [
                'mailer' => $mailerData
            ],
        ]);
    }

    /**
     * Returns a dummy request
     *
     * @return Request
     */
    protected function buildRequest(): Request
    {
        return new Request(['REQUEST_URI' => '/']);
    }

    /**
     * Tests with minimal config
     *
     * @return void
     */
    public function testMinimalConfig(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig([]));
        $this->assertInstanceOf(Mailer::class, $mailer);
    }

    /**
     * Tests with invalid transport method
     *
     * @return void
     */
    public function testIncorrectTransport(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid mailer transport: unknown');
        new Mailer($this->buildRequest(), $this->buildConfig(['transport' => 'unknown']));
    }

    /**
     * Tests with smtp transport
     *
     * @return void
     */
    public function testSmtpConfig(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig([
            'transport' => 'smtp',
            'smtp_hostname' => 'localhost',
            'smtp_authentication' => true,
            'smtp_username' => 'Foo',
            'smtp_password' => 'Bar',
            'smtp_encryption' => 'PLAIN',
            'smtp_port' => 25,
        ]));
        $this->assertInstanceOf(Mailer::class, $mailer);
    }

    /**
     * Tests with an invalid value for smtp_authentication
     *
     * @return void
     */
    public function testSmtpInvalidAuthenticationValue(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Mailer SMTP authentication value must be a boolean');
        new Mailer($this->buildRequest(), $this->buildConfig([
            'transport' => 'smtp',
            'smtp_hostname' => 'localhost',
            'smtp_authentication' => 1,
            'smtp_username' => 'Foo',
            'smtp_password' => 'Bar',
            'smtp_encryption' => 'PLAIN',
            'smtp_port' => 25,
        ]));
    }

    /**
     * Tests with an invalid value for smtp_encryption
     *
     * @return void
     */
    public function testSmtpInvalidEncryptionValue(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Mailer SMTP encryption must be plain, tls or ssl');
        new Mailer($this->buildRequest(), $this->buildConfig([
            'transport' => 'smtp',
            'smtp_hostname' => 'localhost',
            'smtp_authentication' => true,
            'smtp_username' => 'Foo',
            'smtp_password' => 'Bar',
            'smtp_encryption' => 'VLIEGTUIG',
            'smtp_port' => 25,
        ]));
    }

    /**
     * Returns all default ports for encryption methods
     *
     * @return array[]
     */
    public function smtpEncryptionMethods(): array
    {
        return array(
            ['encryption' => 'PLAIN', 'port' => 25],
            ['encryption' => 'TLS',   'port' => 587],
            ['encryption' => 'SSL',   'port' => 465],
        );
    }

    /**
     * Tests default ports
     *
     * @param string  $encryption Encryption method.
     * @param integer $port       Default port.
     *
     * @return       void
     * @dataProvider smtpEncryptionMethods
     */
    public function testSmtpDefaultPorts(string $encryption, int $port): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig([
            'transport' => 'smtp',
            'smtp_hostname' => 'localhost',
            'smtp_authentication' => true,
            'smtp_username' => 'Foo',
            'smtp_password' => 'Bar',
            'smtp_encryption' => $encryption,
        ]));
        $this->assertInstanceOf(Mailer::class, $mailer);

        // Validate if the property is set correctly
        $reflectionProperty = new \ReflectionProperty(Mailer::class, 'smtp');
        $reflectionProperty->setAccessible(true);
        $this->assertEquals($port, $reflectionProperty->getValue($mailer)['port']);
    }

    /**
     * Tests a successful send call, with sendmail transport, html body, attachments, CC, BCC
     *
     * @return void
     */
    public function testSendSendmailHtmlCcBccAttachments(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig([
            'transport' => 'sendmail',
            'from_email' => 'from@example.com',
            'from_name' => 'From',
            'replyto_name' => 'Replyto',
            'replyto_email' => 'no-reply@example.com',
        ]));

        $recipients = [
            new Recipient('foo@example.com'),
            new Recipient('bar@example.com', 'Bar', Recipient::HEADER_CC),
            new Recipient('baz@example.com', 'Baz', Recipient::HEADER_BCC),
        ];

        $attachments = [
            new Attachment(__FILE__),
            new Attachment(null, 'test.txt', 'text/plain', Attachment::DISPOSITION_INLINE, 'foobar'),
        ];

        $body = new Response('<html lang="en"><body>Mail body</body></html>');

        // Mock the mail() function. No need to test PHPMailer, but all properties set above must exist.
        FunctionMock::mock('PHPMailer\\PHPMailer', 'mail', function ($to, $subject, $message, $additional_headers) {
            return (
                $to == 'Foo <foo@example.com>'
                && $subject == 'Mail subject'
                && strpos($message, 'This is a multi-part message in MIME format.') !== false
                && strpos($message, '<html') !== false
                && strpos($message, 'test.txt') !== false
                && strpos($message, base64_encode('foobar')) !== false
                && strpos($additional_headers, 'Cc: Bar <bar@example.com>') !== false
                && strpos($additional_headers, 'Bcc: Baz <baz@example.com>') !== false
            );
        });

        $mailer->sendMail($recipients, 'Mail subject', $body, true, $attachments);
        $this->assertTrue(true);
    }

    /**
     * Tests a successful send call, with sendmail transport, text body, 1 attachment and options set
     *
     * @return void
     */
    public function testSendSendmailTextAttachmentOptions(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig(['transport' => 'sendmail']));

        $recipient = new Recipient('foo@example.com');
        $attachment = new Attachment(__FILE__);
        $body = new Response('Mail body');

        // Mock the mail() function. No need to test PHPMailer, but all properties set above must exist.
        FunctionMock::mock('PHPMailer\\PHPMailer', 'mail', function ($to, $subject, $message) {
            return (
                $to == 'Foo <foo@example.com>'
                && $subject == 'Mail subject'
                && strpos($message, 'This is a multi-part message in MIME format.') !== false
                && strpos($message, '<html') === false
                && strpos($message, 'MailerTest') !== false
            );
        });

        $mailer->sendMail($recipient, 'Mail subject', $body, false, $attachment, [
            'from_name' => 'From',
            'from_email' => 'from@example.com',
            'replyto_name' => 'Replyto',
            'replyto_email' => 'no-reply@example.com',
        ]);
        $this->assertTrue(true);
    }

    /**
     * Tests a send call with invalid recipient
     *
     * @return void
     */
    public function testSendInvalidRecipient(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig(['from_email' => 'from@example.com']));
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The recipient property must be one or more Recipient models, not string');
        $mailer->sendMail('invalid', 'Mail subject', new Response('Mail body'));
    }

    /**
     * Tests a send call with invalid attachment
     *
     * @return void
     */
    public function testSendInvalidAttachment(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig(['from_email' => 'from@example.com']));
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(
            'The attachment property must be zero, one or more Attachment models, not string'
        );
        $mailer->sendMail(new Recipient('foo@bar.baz'), 'Mail', new Response('Mail body'), false, 'notanattachment');
    }

    /**
     * Tests a send call with invalid option
     *
     * @return void
     */
    public function testSendInvalidOption(): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig(['from_email' => 'from@example.com']));
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option: foo');
        $mailer->sendMail(new Recipient('foo@bar.baz'), 'Mail', new Response('Mail'), false, null, ['foo' => 'bar']);
    }

    /**
     * Tests a send call and automatically populate sender name
     *
     * @return void
     */
    public function testSendSendmailPopulateFromName(): void
    {
        FunctionMock::mock('PHPMailer\\PHPMailer', 'mail', function () {
            return true;
        });
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig(['from_email' => 'from@example.com']));
        $mailer->sendMail(new Recipient('foo@bar.baz'), 'Mail', new Response('Mail'), false);
        $this->assertTrue(true);
    }

    /**
     * Tests SMTP
     *
     * @param string $encryptionMethod The encryption method.
     *
     * @return       void
     * @dataProvider smtpEncryptionMethods
     */
    public function testSendSmtp(string $encryptionMethod): void
    {
        $mailer = new Mailer($this->buildRequest(), $this->buildConfig([
            'from_email' => 'from@example.com',
            'transport' => 'smtp',
            'smtp_hostname' => '0.0.0.0', // causes an error, but we have to go that far to actually test this
            'smtp_port' => '1337',
            'smtp_authentication' => true,
            'smtp_encryption' => $encryptionMethod,
            'smtp_username' => 'foo',
            'smtp_password' => 'bar',
        ]));
        $this->expectException(Exception::class);
        $this->expectExceptionMessageMatches('/^(SMTP connect\(\) failed|SMTP Error\: Could not connect to SMTP)/');
        $mailer->sendMail(new Recipient('foo@bar.baz'), 'Mail', new Response('Mail'), false);
    }
}
