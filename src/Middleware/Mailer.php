<?php

namespace Miniframe\Mailer\Middleware;

use Miniframe\Mailer\Model\Attachment;
use Miniframe\Mailer\Model\Recipient;
use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer extends AbstractMiddleware
{
    /**
     * SMTP config
     *
     * @var array|null
     */
    protected $smtp = null;

    /**
     * Initiates the Mailer middleware
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        $transport = 'sendmail';
        if ($config->has('mailer', 'transport')) {
            $transport = $config->get('mailer', 'transport');
            if (!in_array($transport, ['sendmail', 'smtp'])) {
                throw new \RuntimeException('Invalid mailer transport: ' . $transport);
            }
        }

        // Configure SMTP transport
        if ($transport == 'smtp') {
            $this->smtp = [
                'hostname' => $config->get('mailer', 'smtp_hostname'),
                'encryption' => 'PLAIN',
            ];
            if ($config->has('mailer', 'smtp_authentication')) {
                $this->smtp['authentication'] = $config->get('mailer', 'smtp_authentication');
                if (!is_bool($this->smtp['authentication'])) {
                    throw new \RuntimeException('Mailer SMTP authentication value must be a boolean');
                }
            }
            if ($this->smtp['authentication']) {
                $this->smtp['username'] = $config->get('mailer', 'smtp_username');
                $this->smtp['password'] = $config->get('mailer', 'smtp_password');
            }
            if ($config->has('mailer', 'smtp_encryption')) {
                $this->smtp['encryption'] = strtoupper($config->get('mailer', 'smtp_encryption'));
                if (!in_array($this->smtp['encryption'], ['PLAIN', 'TLS', 'SSL'])) {
                    throw new \RuntimeException('Mailer SMTP encryption must be plain, tls or ssl');
                }
            }
            if ($config->has('mailer', 'smtp_port')) {
                $this->smtp['port'] = $config->get('mailer', 'smtp_port');
                if (!is_numeric($this->smtp['port'])) {
                    throw new \RuntimeException('Mailer SMTP port value must be a numeric value');
                }
            } elseif ($this->smtp['encryption'] == 'PLAIN') {
                $this->smtp['port'] = 25;
            } elseif ($this->smtp['encryption'] == 'TLS') {
                $this->smtp['port'] = 587;
            } elseif ($this->smtp['encryption'] == 'SSL') {
                $this->smtp['port'] = 465;
            }
        }
    }

    /**
     * Sends an email.
     *
     * Valid options are:
     * - from_name
     * - from_email
     * - replyto_name
     * - replyto_email
     * They may also be defined in the ini file as default.
     *
     * @param Recipient|Recipient[]        $recipient      One or more recipients.
     * @param string                       $subject        Subject of the mail.
     * @param Response                     $mailBody       A response object that returns a mail body.
     * @param boolean                      $mailBodyIsHtml Set to false if the mail is plain text.
     * @param Attachment|Attachment[]|null $attachment     None, one or more attachments.
     * @param array                        $options        Additional options.
     *
     * @return void
     * @throws Exception Can throw PHPMailer exceptions.
     */
    public function sendMail(
        $recipient,
        string $subject,
        Response $mailBody,
        bool $mailBodyIsHtml = true,
        $attachment = null,
        array $options = array()
    ): void {
        $phpmailer = new PHPMailer(true);
        $this->configureSmtp($phpmailer);
        $this->configureRecipients($phpmailer, is_array($recipient) ? $recipient : [$recipient]);
        $this->configureBody($phpmailer, $mailBody, $mailBodyIsHtml);
        $this->configureAttachments($phpmailer, is_array($attachment) ? $attachment : [$attachment]);
        $this->configureOptions($phpmailer, $options);
        $phpmailer->Subject = $subject;

        $phpmailer->send();
    }

    /**
     * Configures the SMTP properties for a PHPMailer instance
     *
     * @param PHPMailer $phpmailer The PHPMailer instance.
     *
     * @return void
     */
    protected function configureSmtp(PHPMailer $phpmailer): void
    {
        if (!is_array($this->smtp)) {
            return;
        }
        $phpmailer->isSMTP();
        $phpmailer->Host = $this->smtp['hostname'];
        $phpmailer->SMTPAuth = $this->smtp['authentication'];
        if ($this->smtp['authentication']) {
            $phpmailer->Username = $this->smtp['username'];
            $phpmailer->Password = $this->smtp['password'];
        }
        switch ($this->smtp['encryption']) {
            case 'SSL':
                $phpmailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                break;
            case 'TLS':
                $phpmailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                break;
        }
        $phpmailer->Port = $this->smtp['port'];
    }

    /**
     * Adds the recipients to PHPMailer
     *
     * @param PHPMailer   $phpmailer  The PHPMailer instance.
     * @param Recipient[] $recipients List of recipients.
     *
     * @return void
     */
    protected function configureRecipients(PHPMailer $phpmailer, array $recipients): void
    {
        foreach ($recipients as $recipient) {
            if (!($recipient instanceof Recipient)) {
                throw new \InvalidArgumentException(
                    'The recipient property must be one or more Recipient models, not ' . gettype($recipient)
                );
            }
            switch ($recipient->getHeader()) {
                case Recipient::HEADER_TO:
                    $phpmailer->addAddress($recipient->getEmail(), $recipient->getName());
                    break;
                case Recipient::HEADER_CC:
                    $phpmailer->addCC($recipient->getEmail(), $recipient->getName());
                    break;
                case Recipient::HEADER_BCC:
                    $phpmailer->addBCC($recipient->getEmail(), $recipient->getName());
                    break;
            }
        }
    }

    /**
     * Configures the mail body
     *
     * @param PHPMailer $phpmailer The PHPMailer instance.
     * @param Response  $mailBody  A response object that returns a mail body.
     * @param boolean   $isHtml    Set to false if the mail is plain text.
     *
     * @return void
     */
    protected function configureBody(PHPMailer $phpmailer, Response $mailBody, bool $isHtml): void
    {
        $phpmailer->isHTML($isHtml);
        if ($isHtml) {
            $phpmailer->msgHTML($mailBody->render());
        } else {
            $phpmailer->Body = $mailBody->render();
        }
    }

    /**
     * Configures the attachments.
     *
     * @param PHPMailer    $phpmailer   The PHPMailer instance.
     * @param Attachment[] $attachments List of attachments.
     *
     * @return void
     */
    protected function configureAttachments(PHPMailer $phpmailer, array $attachments): void
    {
        if ($attachments === [null]) {
            return;
        }
        foreach ($attachments as $attachment) {
            if (!($attachment instanceof Attachment)) {
                throw new \InvalidArgumentException(
                    'The attachment property must be zero, one or more Attachment models, not '
                    . gettype($attachment)
                );
            }
            switch ($attachment->getDisposition()) {
                case Attachment::DISPOSITION_INLINE:
                    $disposition = 'inline';
                    break;
                case Attachment::DISPOSITION_ATTACHMENT:
                default:
                    $disposition = 'attachment';
                    break;
            }

            if ($attachment->getFullPath()) {
                $phpmailer->addAttachment(
                    $attachment->getFullPath(),
                    $attachment->getFilename(),
                    PHPMailer::ENCODING_BASE64,
                    $attachment->getContentType(),
                    $disposition
                );
            } else {
                $phpmailer->addStringAttachment(
                    $attachment->getAttachmentData(),
                    $attachment->getFilename(),
                    PHPMailer::ENCODING_BASE64,
                    $attachment->getContentType(),
                    $disposition
                );
            }
        }
    }

    /**
     * Parse additional options.
     *
     * @param PHPMailer $phpmailer The PHPMailer instance.
     * @param array     $options   Additional options.
     *
     * @return void
     */
    protected function configureOptions(PHPMailer $phpmailer, array $options): void
    {
        // Validate options
        foreach (array_keys($options) as $optionKey) {
            if (!in_array($optionKey, ['from_name', 'from_email', 'replyto_name', 'replyto_email'])) {
                throw new \InvalidArgumentException('Invalid option: ' . $optionKey);
            }
        }

        // Default sender (from_email is required, in config or in options)
        $fromEmail = $options['from_email'] ?? $this->config->get('mailer', 'from_email');
        if (isset($options['from_name'])) {
            $fromName = $options['from_name'];
        } elseif ($this->config->has('mailer', 'from_name')) {
            $fromName = $this->config->get('mailer', 'from_name');
        } else {
            $fromName = ucfirst(explode('@', $fromEmail, 2)[0]);
        }
        $phpmailer->setFrom($fromEmail, $fromName);

        // Reply to address
        if (isset($options['replyto_email'])) {
            $replyToEmail = $options['replyto_email'];
        } elseif ($this->config->has('mailer', 'replyto_email')) {
            $replyToEmail = $this->config->get('mailer', 'replyto_email');
        } else {
            $replyToEmail = $fromEmail;
        }
        // Reply to name
        if (isset($options['replyto_name'])) {
            $replyToName = $options['replyto_name'];
        } elseif ($this->config->has('mailer', 'replyto_name')) {
            $replyToName = $this->config->get('mailer', 'replyto_name');
        } else {
            $replyToName = ucfirst(explode('@', $replyToEmail, 2)[0]);
        }
        $phpmailer->addReplyTo($replyToEmail, $replyToName);
    }
}
