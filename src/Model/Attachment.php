<?php

namespace Miniframe\Mailer\Model;

class Attachment
{
    /**
     * Disposition values
     */
    public const
        DISPOSITION_INLINE = 1,
        DISPOSITION_ATTACHMENT = 2;

    /**
     * Full path to the file.
     *
     * @var string|null
     */
    private $fullPath;

    /**
     * Name for within the attachment
     *
     * @var string
     */
    private $filename;

    /**
     * Type of the file.
     *
     * @var string|null
     */
    private $contentType = null;
    /**
     * Disposition for the attachment.
     *
     * @var int
     */
    private $disposition;

    /**
     * Attachment data.
     *
     * @var string|null
     */
    private $attachmentData;

    /**
     * Attachment data model
     *
     * @param string|null $fullPath       Path to the source file. When null, attachmentData and filename must be set.
     * @param string|null $filename       Name for within the attachment.
     * @param string|null $contentType    Type of the file.
     * @param integer     $disposition    One of the DISPOSITION_INLINE or DISPOSITION_ATTACHMENT constants.
     * @param string|null $attachmentData Instead of a fullPath, you can also add the data with this parameter.
     */
    public function __construct(
        string $fullPath = null,
        string $filename = null,
        string $contentType = null,
        int $disposition = self::DISPOSITION_ATTACHMENT,
        string $attachmentData = null
    ) {
        if ($fullPath !== null && !file_exists($fullPath)) {
            throw new \RuntimeException('File does not exist: ' . $fullPath);
        }
        if ($filename === null) {
            $filename = pathinfo($fullPath, PATHINFO_BASENAME);
        }
        if (!in_array($disposition, [self::DISPOSITION_INLINE, self::DISPOSITION_ATTACHMENT])) {
            throw new \InvalidArgumentException(
                'Disposition must be one of DISPOSITION_INLINE or DISPOSITION_ATTACHMENT'
            );
        }

        if ($fullPath === null && (!$filename || !$attachmentData)) {
            throw new \InvalidArgumentException('When no fullPath is given, filename and attachmentData are required.');
        }
        if ($fullPath !== null && $attachmentData) {
            throw new \InvalidArgumentException('When attachmentData is specified, filename must be null.');
        }

        $this->fullPath = $fullPath;
        $this->filename = $filename;
        $this->disposition = $disposition;
        $this->attachmentData = $attachmentData;
        if ($contentType) {
            $this->contentType = $contentType;
        } elseif (function_exists('mime_content_type')) { // ext-fileinfo
            $this->contentType = mime_content_type($fullPath) ?? null;
        }
    }

    /**
     * Returns the full path to the source file.
     *
     * @return string|null
     */
    public function getFullPath(): ?string
    {
        return $this->fullPath;
    }

    /**
     * Returns the name of the attachment.
     *
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Returns the type of the file.
     *
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * Returns the disposition for the attachment.
     *
     * @return integer
     */
    public function getDisposition(): int
    {
        return $this->disposition;
    }

    /**
     * Returns the attachment data.
     *
     * @return string|null
     */
    public function getAttachmentData(): ?string
    {
        return $this->attachmentData;
    }

    /**
     * Magic method; sets this model based on a specific state
     *
     * @param array $properties The actual state.
     *
     * @return Attachment
     * @see    var_export()
     */
    public static function __set_state(array $properties): self
    {
        return new self(
            $properties['fullPath'],
            $properties['filename'],
            $properties['contentType'],
            $properties['disposition'],
            $properties['attachmentData']
        );
    }
}
