<?php

namespace Miniframe\Mailer\Model;

class Recipient
{
    /**
     * Header constants
     */
    public const
        HEADER_TO = 1,
        HEADER_CC = 2,
        HEADER_BCC = 3;

    /**
     * Recipient email address.
     *
     * @var string
     */
    private $email;
    /**
     * Recipient name.
     *
     * @var string
     */
    private $name;
    /**
     * Required mail header
     *
     * @var int
     */
    private $header;

    /**
     * Recipient data model
     *
     * @param string      $email  Email address of the recipient.
     * @param string|null $name   Name of the recipient.
     * @param integer     $header One of the Header constants (self::HEADER_TO, self::HEADER_CC, self::HEADER_BCC).
     */
    public function __construct(string $email, string $name = null, int $header = self::HEADER_TO)
    {
        if (!in_array($header, [self::HEADER_TO, self::HEADER_CC, self::HEADER_BCC])) {
            throw new \InvalidArgumentException(
                'The header must be one of the constants HEADER_TO, HEADER_CC or HEADER_BCC'
            );
        }
        if (!trim($email) || !filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Invalid email address specified');
        }

        $this->email = trim($email);
        $this->name = trim($name ?? ucfirst(explode('@', $email, 2)[0]));
        $this->header = $header;
    }

    /**
     * Returns the mail address.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Returns the name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the header.
     *
     * @return integer
     */
    public function getHeader(): int
    {
        return $this->header;
    }

    /**
     * Magic method; sets this model based on a specific state
     *
     * @param array $properties The actual state.
     *
     * @return Recipient
     * @see    var_export()
     */
    public static function __set_state(array $properties): self
    {
        return new self($properties['email'], $properties['name'], $properties['header']);
    }
}
