<?php
namespace PHPSTORM_META {

    // Suggests constants as 3rd argument in Recipient::__construct()
    expectedArguments(
        \Miniframe\Mailer\Model\Recipient::__construct(),
        2,
        \Miniframe\Mailer\Model\Recipient::HEADER_TO,
        \Miniframe\Mailer\Model\Recipient::HEADER_CC,
        \Miniframe\Mailer\Model\Recipient::HEADER_BCC
    );
    expectedReturnValues(\Miniframe\Mailer\Model\Recipient::getHeader(),
        \Miniframe\Mailer\Model\Recipient::HEADER_TO,
        \Miniframe\Mailer\Model\Recipient::HEADER_CC,
        \Miniframe\Mailer\Model\Recipient::HEADER_BCC
    );

    // Suggests constants as 4td argument in Attachment::__construct()
    expectedArguments(
        \Miniframe\Mailer\Model\Attachment::__construct(),
        3,
        \Miniframe\Mailer\Model\Attachment::DISPOSITION_INLINE,
        \Miniframe\Mailer\Model\Attachment::DISPOSITION_ATTACHMENT
    );
    expectedReturnValues(\App\Model\Attachment::getDisposition(),
        \Miniframe\Mailer\Model\Attachment::DISPOSITION_INLINE,
        \Miniframe\Mailer\Model\Attachment::DISPOSITION_ATTACHMENT
    );
}
