# Miniframe Mailer Bundle

This is a mailer bundle for the [Miniframe PHP Framework](https://miniframe.dev/).

With this bundle, it's easy to set up outgoing emails from within your PHP application.

[![build](https://miniframe.dev/build/badge/mailer-bundle)](https://bitbucket.org/miniframe/mailer-bundle/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/mailer-bundle)](https://miniframe.dev/codecoverage/mailer-bundle)

## How to use

1. In your existing project, type: `composer require miniframe/mailer-bundle`
2. Add the directive below to your config.ini

*Example config.ini directives*
```ini
[framework]
middleware[] = Miniframe\Mailer\Middleware\Mailer

[mailer]
from_name = Sender name
from_email = no-reply@foobar.baz
; Optionally, to use SMTP:
transport = smtp
smtp_hostname = smtp.foobar.baz
smtp_authentication = true
smtp_username = foobar
smtp_port = 587
; SMTP encryption can be TLS, SSL or PLAIN
smtp_encryption = TLS

```
*Place the password in a separate `secrets.ini` that's in your `.gitignore` file:*
```ini
[mailer]
smtp_password = ************************
```

Now, you can use this in your code:

```php
$recipient = new Recipient('email@example.com', 'Example');
$subject = 'Mail subject';
$mailBody = new Response('<html lang="en"><body><b>HTML</b> mail</body></html>');

$mailer = Registry::get(Mailer::class);
$mailer->sendMail($recipient, $subject, $mailBody);
```

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/mailer-bundle/addon/pipelines/home) to automatically test this project.
